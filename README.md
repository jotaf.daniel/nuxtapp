# Nuxt App

## Running local

- **dev server** `yarn dev`
- **prod server** `yarn start`

## Emulating deployment

> this section assumes you have Docker and Docker-Compose installed
> and properly set (daemon running, user added to `docker` group, etc)

1. generate static app `yarn generate`
2. run docker nginx `docker-compose up --build`
3. open `localhost:8080`
4. try refresh without any Internet connection


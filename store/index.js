export const state = () => ({
  payload: [],
});

export const getters = {
  payload: (s) => s.payload,
};

export const mutations = {
  setPayload: (s, payload) => (s.payload = payload),
};

export const actions = {
  nuxtServerInit: async ({ commit }) => {
    const res = await fetch("https://dog.ceo/api/breeds/list/all");
    const { message } = await res.json();

    commit("setPayload", message);
  },
};
